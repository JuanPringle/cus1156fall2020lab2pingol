import java.util.ArrayList;

public class CatManager
{
	private ArrayList<Cat>myCats=new ArrayList<Cat>();

	public CatManager()
	{
		Cat cat = new Cat("Fifi", "black");
		myCats.add(cat);
		cat = new Cat("Fluffy", "spotted");
		myCats.add(cat);
		cat = new Cat("Josephine", "tabby");
		myCats.add(cat);
		cat = new Cat("Biff", "tabby");
		myCats.add(cat);
		cat = new Cat("Bumpkin", "white");
		myCats.add(cat);
		cat = new Cat("Spot", "spotted");
		myCats.add(cat);
		cat = new Cat("Lulu", "tabby");
		myCats.add(cat);
	}

	
	public void add(Cat aCat)
	{
		myCats.add(aCat);
	}


	public Cat findThisCat(String name)
	{
		for(int i=0; myCats.size()>i;i++) {
			if(myCats.get(i).getName().equals(name)){
				return myCats.get(i); 
			}
		}
		return null;
	}
	
	public int countColors(String color)
	{
		int counter= 0;
		for(int i=0;myCats.size()>i;i++) {
			if(myCats.get(i).getColor().equals(color)) {
				counter++;
			}
		}
		return counter;

	}
}
